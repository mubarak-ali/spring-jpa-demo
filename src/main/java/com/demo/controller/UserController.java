package com.demo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.demo.entity.User;
import com.demo.repository.UserRepository;

@RestController
@RequestMapping("api/v1/user")
public class UserController {

	@Autowired
	UserRepository _user;
	
	@RequestMapping(value = "", method = RequestMethod.GET)
	public List<User> GetAllUsers() {
		return (List<User>) _user.GetAllUsers();
	}
	
	@RequestMapping(value = "", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public void InsertUser(User user) {
		_user.saveAndFlush(user);
	}
	
}
