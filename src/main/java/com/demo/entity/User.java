package com.demo.entity;

import java.util.Date;
import java.util.List;

import javax.persistence.*;

@Entity(name = "app_user")
public class User {
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "user_generator")
	@SequenceGenerator(name = "user_generator", sequenceName = "user_pkid_seq")
	Integer id;
	
	String username;
	
	String email;
	
	String password;
	
	@Column(name = "last_login", columnDefinition = "timestamp with time zone")
	@Temporal(TemporalType.TIMESTAMP)
	Date lastLogin;
	
	@ManyToMany(cascade = CascadeType.ALL)
	@JoinTable(
			name = "app_users_in_groups", 
			joinColumns = { @JoinColumn(name = "user_id", referencedColumnName = "id")}, 
			inverseJoinColumns = {@JoinColumn(name = "group_id", referencedColumnName = "id")})
	List<Group> groups;
	
	User(){}
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public Date getLastLogin() {
		return lastLogin;
	}
	public void setLastLogin(Date lastLogin) {
		this.lastLogin = lastLogin;
	}
	
}
