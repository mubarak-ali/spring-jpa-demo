package com.demo.repository;

import java.util.List;

import com.demo.entity.User;

public interface UserRepositoryExt {

	List<User> GetAllUsers();
}
