package com.demo.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.demo.entity.Group;

public interface GroupRepository extends JpaRepository<Group, Integer> {
	
}
