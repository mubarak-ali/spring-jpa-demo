package com.demo.repository.implementation;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import com.demo.entity.User;
import com.demo.repository.UserRepositoryExt;

public class UserRepositoryImpl implements UserRepositoryExt {
	
	@PersistenceContext
	EntityManager _em;
	
	@SuppressWarnings("unchecked")
	public List<User> GetAllUsers() {
		return _em.createQuery("from app_user").getResultList();
	}
}
