package com.demo.config;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Component;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

@Component
public class MyAuthInterceptor implements HandlerInterceptor {

	@Override
	public void afterCompletion(HttpServletRequest arg0, HttpServletResponse arg1, Object arg2, Exception arg3)
			throws Exception {
		
//		System.out.println("________________________________________");
//		System.out.println("In afterCompletion Request Completed");
//		System.out.println("________________________________________");
	}

	@Override
	public void postHandle(HttpServletRequest arg0, HttpServletResponse arg1, Object arg2, ModelAndView arg3)
			throws Exception {

//		System.out.println("_________________________________________");
//		System.out.println("In postHandle request processing "
//				+ "completed by @RestController");
//		System.out.println("_________________________________________");		
	}

	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object data) 
			throws Exception {

		System.out.println("In preHandle we are Intercepting the Request");
		System.out.println("____________________________________________");
		String url = request.getRequestURI();
		Integer personId = ServletRequestUtils.getIntParameter(request, "personId", 0);
		System.out.println("RequestURI::" + url + 
				" || Search for Person with personId ::" + personId);
		System.out.println("____________________________________________");
		return true;
	}
}
